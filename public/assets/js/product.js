



$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
// console.log($('meta[name="csrf-token"]').attr('content'))
$(document).ready(function (){
    $('.add-to-cart').click(() => {
        //add to cart with ajax 
        let productId = $('#productId').val()
        let productQty = $('#productQty').val()
        $.ajax({
            type: "post",
            url: '/add-cart',
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                'pId': productId,
                'qty': productQty,
            },
            success: function (data){ 
                // console.log(data)
                window.location.href='/' 
                },
            dataType: 'json'
        });
    });
    $(document).on('click', '.btn-increase', function(event)  {
        event.preventDefault();
        let productId = $(this).parent().find('.productId').val()
        let qty = $(this).parent().find('.product-quantity').val()
        let new_qty = parseInt(qty) +1;
        let element = this
        changeQuantity(element, productId, new_qty)
    })
    $(document).on('click', '.btn-reduce', function(event)  {
        event.preventDefault();
        let productId = $(this).parent().find('.productId').val()
        let qty = $(this).parent().find('.product-quantity').val()
        let new_qty = parseInt(qty) - 1;
        let element = this
        changeQuantity(element, productId, new_qty)
    })
    function changeQuantity (element, productId, new_qty){
        let price = $(element).parent().find('.product-price').val()
        let total_pro = parseInt(new_qty)* parseInt(price);
        let new_sub = 0;
        $(element).parent().find('.product-quantity').val(new_qty)
        $(element).parent().parent().parent().find('.sub-total').find('.price').find('.value').text(total_pro)
        $(element).parent().parent().parent().find('.sub-total').attr('data-totalpro',total_pro)
        $(element).parent().find('.total-pro').val(total_pro)
        $('.total-pro').each((index, el) => {
            new_sub+=parseFloat($(el).val())
        })
        $('.total').text(new_sub)
        $.ajax({
            type: "post",
            url: '/chang-cart-quantity',
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                'pId': productId,
                'quantity': new_qty,
            },
            success: function (data){
                if (data.status === 1) {
                }
            },
            dataType: 'json'
        });
    }
    $('.btn-delete-item').click(function(e){
        e.preventDefault();
        let new_sub = 0;
        pid = $(this).data("id");
        $(this).parent().parent().remove()
        $('.total-pro').each((index, el) => {
            new_sub+=parseFloat($(el).val())
        })
        $('.total').text(new_sub)
        $.ajax({
            type:'POST',
            url:'/delete-cart-item',
            data:
            {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                 pid:pid 
            },
            dataType: 'json',
            success:function(data){
                alert('Delete success');
            }
        });
    });
})