@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Config Web</h3>
  
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 3%" class="text-center">
                          Id
                      </th>
                      <th style="width: 10%" class="text-center">
                          Web Name
                      </th>
                      <th style="width: 10%" class="text-center">
                          Logo
                      </th>
                      <th style="width: 10%" class="text-center">
                          Facebook
                      </th>
                      <th style="width: 10%" class="text-center">
                          Zalo
                      </th>
                      <th style="width: 10%" class="text-center">
                          Address
                      </th>
                      <th style="width: 10%" class="text-center">
                        Email
                      </th>
                      <th style="width: 10%" class="text-center">
                        Actions
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($config as $item)
                  <tr>
                    <td class="text-center">
                        {{ $item->id }}
                    </td>
                    <td class="text-center">
                        {{ $item->webname }}
                    </td>
                    @if ( $item->logo != null)
                        <td class="text-center">
                            <img src="{{ asset('/images/' . $item->logo) }}" alt="{{ $item->logo }}" style="width: 100px; height: auto" >
                        </td>
                    @endif
                    <td class="text-center">
                        {{ $item->facebook }}
                    </td>
                    <td class="text-center">
                        {{ $item->zalo }}
                    </td>
                    <td class="text-center">
                        {{ $item->address }}
                    </td>
                    <td class="text-center">
                        {{ $item->email }}
                    </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-info btn-sm" href="{{ route('config.edit', $item->id) }}">
                              <i class="fas fa-pencil-alt">
                              </i>
                          </a>
                          <form style="display:inline-block" action="{{ route('config.destroy', $item->id) }}" method="POST">
                            @method("DELETE")
                            @csrf
                            <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
                          </form>
                      </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection