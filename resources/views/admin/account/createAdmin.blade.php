@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <div class="row">
        <div class="col-lg-11 col-sm-11 col-md-11 col-xs-11 bg-white ml-5">							
            <div class="card">
                <div class="card-body">
                    <div class="register-form form-item">
                        <form class="form-stl" action="{{ route('account.store') }}" name="frm-login" method="post"  >
                            @csrf
                            <div class="wrap-title">
                                <h3 class="form-title">Create Admin</h3>
                            </div>	
                            <div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Username</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="username" required placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Password</label>
                                    <input type="password" class="form-control" id="frm-reg-lname" min="6" name="password"required placeholder="*******">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Name</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="name"required placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Gender</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="gender"  required placeholder="Gender">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Address</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="address"  required placeholder="Address">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Phone</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="phone" required placeholder="Phone">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Email</label>
                                    <input type="email" class="form-control" id="frm-reg-lname" name="email"  required placeholder="Email">
                                </div>
                                <div class="col-md-12" style="text-align: right">
                                    <input type="submit" class="btn btn-sign btn-primary" value="Create">
                                </div>
                            </div>
                        </form>
                    </div>											
                </div>
            </div><!--end main products area-->		
        </div>
    </div>
@endsection