@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <div class="row">
        <div class="col-lg-11 col-sm-11 col-md-11 col-xs-11 bg-white ml-5">							
            <div class="card">
                <div class="card-body">
                    <div class="register-form form-item">
                        <form class="form-stl" action="{{ route('account.update', Auth::user()->id) }}" name="frm-login" method="post"  >
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="wrap-title">
                                <h3 class="form-title">Update Admin</h3>
                            </div>	
                            <div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Username</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="username" value="{{ $users->username ??''}}"required placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Name</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="name" value="{{ $users->name ??''}}" required placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Gender</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="gender" value="{{ $users->gender ??''}}" required placeholder="Gender">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Address</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="address" value="{{ $users->address ??''}}" required placeholder="Address">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Phone</label>
                                    <input type="text" class="form-control" id="frm-reg-lname" name="phone" value="{{ $users->phone ??''}}" required placeholder="Phone">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Email</label>
                                    <input type="email" class="form-control" id="frm-reg-lname" name="email" value="{{ $users->email ??''}}" required placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="frm-reg-lname">Roles</label>
                                    <select id="inputClientCompany" class="form-control"  name="roles" required class="form-control custom-select">
                                        <option selected disabled>{{ $users->roles  ??''}}</option>
                                            <option value="admin">admin</option>
                                    </select>
                                </div>
                                <div class="col-md-12" style="text-align: right">
                                    <input type="submit" class="btn btn-sign btn-primary" value="Update" name="register">
                                </div>
                            </div>
                        </form>
                    </div>											
                </div>
            </div><!--end main products area-->		
        </div>
    </div>
@endsection