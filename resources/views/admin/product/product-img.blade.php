@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Product Image</h3>
  
          <div class="card-tools">
            <a href="{{ route('product-img.create') }}" class="btn btn-tool">Add image</a>
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%" class="text-center">
                          Id
                      </th>
                      <th style="width: 10%" class="text-center">
                        Product Id
                      </th>
                      <th style="width: 5%" class="text-center">
                        Image
                      </th>
                      <th style="width: 3%" class="text-center">
                        Actions
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($product_img as $item)
                  <tr>
                    <td class="text-center">
                      {{ $item->id }}
                  </td>
                    <td class="text-left">
                        {{ $item->Prod->name ??''}}
                    </td>
                    @if ( $item->image != null)
                        <td class="text-center">
                            <img src="{{ asset('/productImg/' . $item->image) }}" alt="{{ $item->image}}" style="width: 100px; height: auto" >
                        </td>
                    @endif
                    <td class="project-actions text-center">
                      <a href="{{ route('product-img.edit', $item->id) }}" class="btn btn-info btn-sm">
                        <i class="fas fa-pencil-alt"></i>
                      </a>
                      <form style="display:inline-block" action="{{ route('product-img.destroy', $item->id) }}" method="POST">
                        @method("DELETE")
                        @csrf
                        <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
                      </form>
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
    </div>
@endsection