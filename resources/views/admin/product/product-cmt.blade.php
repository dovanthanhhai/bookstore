@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Product Comment</h3>
  
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%" class="text-center">
                          Id
                      </th>
                      <th style="width: 4%" class="text-center">
                        Customer 
                      </th>
                      <th style="width: 10%" class="text-center">
                        Product
                      </th>
                      <th style="width: 8%" class="text-center">
                        Content
                      </th>
                      {{-- <th style="width: 5%" class="text-center">
                        Actions
                      </th> --}}
                  </tr>
              </thead>
              <tbody>
                @foreach ($product_cmt as $item)
                  <tr>
                    <td class="text-center">
                      {{ $item->id }}
                  </td>
                  <td class="text-center">
                      {{ $item->Username->name ??'' }}
                  </td>
                    <td class="text-left">
                        {{ $item->Product->name }}
                    </td>
                    <td class="text-center">
                        {{ $item->content }}
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection