@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Slider</h3>
  
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 10%" class="text-center">
                          Id
                      </th>
                      <th style="width: 40%" class="text-center">
                          Image
                      </th>
                      <th style="width: 25%" class="text-center">
                          Url
                      </th>
                      <th style="width: 25%" class="text-center">
                        Actions
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($slider as $item)
                  <tr>
                    <td class="text-center">
                        {{ $item->id }}
                    </td>
                    @if ( $item->image != null)
                        <td class="text-center">
                            <img src="{{ asset('/slider/' . $item->image) }}" alt="{{ $item->image }}" style="width: 100px; height: auto" >
                        </td>
                    @endif
                    <td class="text-center">
                        {{ $item->url }}
                    </td>
                      <td class="project-actions text-right">
                          <a class="btn btn-info btn-sm" href="{{ route('sliders.edit', $item->id) }}">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <form style="display:inline-block" action="{{ route('sliders.destroy', $item->id) }}" method="POST">
                            @method("DELETE")
                            @csrf
                            <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
                          </form>
                      </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection