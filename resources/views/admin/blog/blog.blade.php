@extends('admin.layout.layout')
@section('title',$title ?? '')
@section('content')
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title"> Blog</h3>
          <div class="card-tools row">
            <div class="col-md-5" style="width: 100px">
              <a href="{{ route('product.create') }}">Create</a>
            </div>
            <button type="button" class="btn btn-tool col-md-2" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool col-md-2" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 2%" class="text-center">
                          Id
                      </th>
                      <th style="width: 10%" class="text-center">
                        Name
                      </th>
                      <th style="width: 3%" class="text-center">
                          Price
                      </th>
                      <th style="width: 5%" class="text-center">
                          Price Sale
                      </th>
                      <th style="width: 8%" class="text-center">
                        Image
                      </th>
                      <th style="width: 0%" class="text-center">
                        Actions
                      </th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($blog as $item)
                  <tr>
                    <td class="text-center" style="font-size: 14px">
                      {{ $item->id }}
                    </td>
                    <td class="text-center" style="font-size: 14px">
                        {{ $item->name }}
                    </td>
                    <td class="text-center" style="font-size: 14px">
                      <del> ${{ number_format($item->price) }} </del>
                    </td>
                    <td class="text-center" style="font-size: 14px">
                      ${{ number_format($item->price_sale) }}
                      </td>
                    @if ( $item->image_1 != null)
                        <td class="text-center">
                            <img src="{{ asset('/products/' . $item->image_1) }}" alt="{{ $item->image_1 }}" style="width: 65px; height: auto" >
                        </td>
                    @endif
                    <td class="text-center text-right" style="font-size: 14px">
                      <a href="{{ route('product.edit', $item->id) }}" class="btn btn-info btn-sm">
                        <i class="fas fa-pencil-alt"></i>
                      </a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
    </div>
@endsection