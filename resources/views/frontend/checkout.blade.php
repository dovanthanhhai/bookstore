@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')
            <!--main area-->
    <main id="main" class="main-site">

        <div class="container">

            <div class="wrap-breadcrumb">
                <ul>
                    <li class="item-link"><a href="#" class="link">home</a></li>
                    <li class="item-link"><span>Checkout</span></li>
                </ul>
            </div>
            <div class=" main-content-area">
                <div class="wrap-address-billing">
                    <h3 class="box-title">Order</h3>
                    <form action="{{ route('orders-store') }}" method="post" name="frm-billing">
                        @csrf
                        <p class="row-in-form">
                            <label for="fname">Name<span>*</span></label>
                            <input id="fname" type="text" name="user_name" value="{{ $user->name ?? ''}}" placeholder="Name">
                        </p>
                        <p class="row-in-form">
                            <label for="lname">SĐT:<span>*</span></label>
                            <input id="phone" type="text" name="user_phone" value="{{ $user->phone ?? '' }}" placeholder="Phone">
                        </p>
                        <p class="row-in-form">
                            <label for="email"> Address<span>*</span></label>
                            <input id="add" type="text" name="user_address" value="{{ $user->address ?? '' }}" placeholder="Address">
                        </p>
                        <p class="row-in-form">
                            <label for="phone"> Email Address<span>*</span></label>
                            <input id="email" type="email" name="user_email" value="{{ $user->email ?? '' }}" placeholder="Email">
                        </p>
                        <p class="row-in-form">
                            <label for="add">Total: <span>${{ number_format($total) }}</span></label>
                            <input id="total" type="hidden" name="total_price" value="{{ $total }}">
                        </p>
                        <p >
                            <label for="country">Payment
                                <div class="choose-payment-methods">
                                    <label class="payment-method">
                                        <input name="payment-method" id="payment-method-visa" value="visa" type="radio">
                                        <span>visa</span>
                                    </label>
                                    <label class="payment-method">
                                        <input name="payment-method" id="payment-method-bank" value="bank" type="radio" checked>
                                        <span>after receiving the goods</span>
                                    </label>
                                </div>
                            </label>
                        </p>
                        <button type="submit" class="btn btn-success">Confirm</button>
                    </form>
                </div>
            </div><!--end main content area-->
        </div><!--end container-->

    </main>
    <!--main area-->
@endsection

