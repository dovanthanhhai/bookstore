@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')
    <div class="order" style="padding: 20px;height: 360px;">
        <div class="order-item" style="padding: 50px">
            @foreach ($order->orderDetail as $item)
                <div class="col-md-2"  style="padding: 10px 0px">
                    <img src="{{ asset('/products/' . $item->Products->image_1 ??'') }}" alt="{{ $item->Products->image_1 ??'' }}" style="width: 150px; height: auto; float:right" >
                </div>
                <div class="col-md-4 " style="padding: 20px 0px">
                    <ul style="padding-left: 20px;">
                    <li style="list-style: none;margin: 6px 0px;"><span style="font-size: 14px; font-weight: 700;">ID:</span> {{ $item->id }}</li>
                    <li style="list-style: none;margin: 6px 0px;"><span style="font-size: 14px; font-weight: 700;">Order Id: </span>{{ $item->orders_id }}</li>
                    <li style="list-style: none;margin: 6px 0px;"><span style="font-size: 14px; font-weight: 700;">Product: </span>{{ $item->Products->name ??''}}</li>
                    <li style="list-style: none;margin: 6px 0px;"><span style="font-size: 14px; font-weight: 700;">Price : </span>${{ number_format($item->price) }}</li>
                    <li style="list-style: none;margin: 6px 0px;"><span style="font-size: 14px; font-weight: 700;">Quantity: </span>{{ $item->quantity }}</li>
                    </ul>
                </div>
            @endforeach
        </div>
    </div>
@endsection