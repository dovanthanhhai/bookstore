@extends('frontend.layout.layout')
@section('title',$title ?? '')  
@section('content')
<div class="row">
    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 col-md-offset-3">							
        <div class=" main-content-area">
            <div class="wrap-login-item ">
                <div class="register-form form-item">
                    <form class="form-stl" action="{{ route('customer.update', Auth::user()->id) }}" name="frm-login" method="post"  >
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="wrap-title">
                            <h3 class="form-title">Update my account</h3>
                        </div>	
                        <div class="row">
                            <div class="wrap-input col-md-12">
                                <label for="frm-reg-lname">Username</label>
                                <input type="text" id="frm-reg-lname" name="username" value="{{ $users->username }}" required placeholder="Username">
                            </div>
                            <div class="wrap-input col-md-12">
                                <label for="frm-reg-lname">Name</label>
                                <input type="text" id="frm-reg-lname" name="name" value="{{ $users->name }}" required placeholder="Name">
                            </div>
                            <div class="wrap-input col-md-12">
                                <label for="frm-reg-lname">Gender</label>
                                <input type="text" id="frm-reg-lname" name="gender" value="{{ $users->gender }}" required placeholder="Gender">
                            </div>
                            <div class="wrap-input col-md-12">
                                <label for="frm-reg-lname">Address</label>
                                <input type="text" id="frm-reg-lname" name="address" value="{{ $users->address }}" required placeholder="Address">
                            </div>
                            <div class="wrap-input col-md-12">
                                <label for="frm-reg-lname">Phone</label>
                                <input type="text" id="frm-reg-lname" name="phone" value="{{ $users->phone }}" required placeholder="Phone">
                            </div>
                            <div class="wrap-input col-md-12">
                                <label for="frm-reg-lname">Email</label>
                                <input type="email" id="frm-reg-lname" name="email" value="{{ $users->email }}" required placeholder="Email">
                            </div>
                            <div class="col-md-12" style="text-align: right">
                                <input type="submit" class="btn btn-sign" value="Update">
                            </div>
                        </div>
                    </form>
                </div>											
            </div>
        </div><!--end main products area-->		
    </div>
</div>
@endsection