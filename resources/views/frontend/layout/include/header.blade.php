<header id="header" class="header header-style-1">
    <div class="container-fluid">
        <div class="row">
            <div class="topbar-menu-area">
                <div class="container">
                    <div class="topbar-menu left-menu">
                        <ul>
                            <li class="menu-item">
                                <a title="Hotline: 0837418189" href="tel:0837418189" ><span class="icon label-before fa fa-mobile"></span>Hotline: +84 837 418 189</a>
                            </li>
                        </ul>
                    </div>
                    <div class="topbar-menu right-menu">
                        <ul>
                            @if (empty($login))
                                <li class="menu-item" ><a title="Register or Login" href="{{ route('login.index') }}">Login</a></li>
                                <li class="menu-item" ><a title="Register or Login" href="{{ route('user.create') }}">Register</a></li>
                            @else
                                <li class="menu-item lang-menu menu-item-has-children parent">
                                    <a title="English" href="#"><span class="img label-before"><img src="{{ asset('assets/images/human.png') }}" style="height: 20px; width: 20px;" alt="lang-en"></span><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    <ul class="submenu curency">
                                        <li class="menu-item" >
                                            <a title="My Account" href="{{ route('my-account') }}">My Account</a>
                                        </li>
                                        <li class="menu-item" >
                                            <a title="Purchase form" href="{{ route('history') }}">
                                                <div>
                                                    <div style="float: right">
                                                        {{$order ?? ''}}
                                                    </div>
                                                </div>
                                                <p style="margin-top: 20px;">Purchase form</p>
                                            </a>
                                        </li>
                                        <li class="menu-item" >
                                            <form action="{{ route('logout') }}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-primary">Logout</button>
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                            {{-- <li class="menu-item lang-menu menu-item-has-children parent">
                                <a title="English" href="#"><span class="img label-before"><img src="{{ asset('assets/images/lang-en.png') }}" alt="lang-en"></span>English<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                <ul class="submenu lang" >
                                    <li class="menu-item" ><a title="hungary" href="#"><span class="img label-before"><img src="{{ asset('assets/images/lang-vietnam.png') }}" alt="lang-hun"></span>Vietnames</a></li>
                                    <li class="menu-item" ><a title="hungary" href="#"><span class="img label-before"><img src="{{ asset('assets/images/lang-hun.png') }}" alt="lang-hun"></span>Hungary</a></li>
                                    <li class="menu-item" ><a title="german" href="#"><span class="img label-before"><img src="{{ asset('assets/images/lang-ger.png') }}" alt="lang-ger" ></span>German</a></li>
                                    <li class="menu-item" ><a title="french" href="#"><span class="img label-before"><img src="{{ asset('assets/images/lang-fra.png') }}" alt="lang-fre"></span>French</a></li>
                                    <li class="menu-item" ><a title="canada" href="#"><span class="img label-before"><img src="{{ asset('assets/images/lang-can.png') }}" alt="lang-can"></span>Canada</a></li>
                                </ul>
                            </li> --}}
                            {{-- <li class="menu-item menu-item-has-children parent" >
                                <a title="Dollar (USD)" href="#">Dollar (USD)<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                <ul class="submenu curency" >
                                    <li class="menu-item" >
                                        <a title="Euro (EUR)" href="#">VietNam (VND)</a>
                                    </li>
                                    <li class="menu-item" >
                                        <a title="Euro (EUR)" href="#">Euro (EUR)</a>
                                    </li>
                                    <li class="menu-item" >
                                        <a title="Dollar (USD)" href="#">Dollar (USD)</a>
                                    </li>
                                </ul>
                            </li> --}}
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="mid-section main-info-area">
                    <div class="wrap-logo-top left-section">
                        
                        @foreach ($config as $item)
                            @if ( $item->logo != null)
                                <a href="#" class="link-to-home"><img src="{{ asset('/images/' . $item->logo) }}" style="height: 62px; width: 130px;" alt="{{ $item->logo }}"></a>
                            @endif
                        @endforeach
                    </div>
                    <div class="wrap-search center-section">
                        <div class="wrap-search-form">
                            <form action="{{route('search')}}" id="form-search-top" method="GET">
                                <input type="text" name="key" class="inputsearch" placeholder="Search here...">
                                <button form="form-search-top" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                <div class="wrap-list-cate">
                                    <select name="category" class="list-cate" id="">
                                        <option class="level-0" value="0">All Category</option>
                                        @if (!empty($categories))
                                            @foreach ($categories as $cate)
                                                <option class="level-0" value="{{ $cate->id }}">{{ $cate->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="wrap-icon right-section">
                        <div class="wrap-icon-section minicart">
                            <a href="{{ route('cart') }}" class="link-direction">
                                <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                                <div class="left-info">
                                    @if (!empty($cart_number))
                                        <span class="index">
                                            {{$cart_number}}
                                        </span>
                                    @endif
                                </div>
                            </a>
                        </div>
                        <div class="wrap-icon-section show-up-after-1024">
                            <a href="#" class="mobile-navigation">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            
             <div class="nav-section header-sticky">
                <div class="header-nav-section">
                    <div class="container">
                        <ul class="nav menu-nav clone-main-menu" id="mercado_haead_menu" data-menuname="Sale Info" >
                            <!-- <li class="menu-item"><a href="#" class="link-term">Weekly Featured</a><span class="nav-label hot-label">hot</span></li>
                            <li class="menu-item"><a href="#" class="link-term">Hot Sale items</a><span class="nav-label hot-label">hot</span></li>
                            <li class="menu-item"><a href="#" class="link-term">Top new items</a><span class="nav-label hot-label">hot</span></li>
                            <li class="menu-item"><a href="#" class="link-term">Top Selling</a><span class="nav-label hot-label">hot</span></li>
                            <li class="menu-item"><a href="#" class="link-term">Top rated items</a><span class="nav-label hot-label">hot</span></li>
                        </ul> -->
                    </div>
                </div> 

                <div class="primary-nav-section">
                    <div class="container">
                        <ul class="nav primary clone-main-menu" id="mercado_main" data-menuname="Main menu" >
                            <li class="menu-item home-icon">
                                <a href="{{ route('index') }}" class="link-term mercado-item-title"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="menu-item">
                                <a href="{{ route('book') }}" class="link-term mercado-item-title">Book</a>
                            </li>
                            <li class="menu-item">
                                <a href="{{ route('comic') }}" class="link-term mercado-item-title">comic</a>
                            </li>
                            <li class="menu-item">
                                <a href="{{ route('magazine') }}" class="link-term mercado-item-title">magazing</a>
                            </li>
                            <li class="menu-item">
                                <a href="{{ route('stationery') }}" class="link-term mercado-item-title">stationery</a>
                            </li>
                            <li class="menu-item">
                                <a href="{{ route('promotion') }}" class="link-term mercado-item-title">promotion</a>
                            </li>	
                            <li class="menu-item">
                                <a href="{{ route('blog') }}" class="link-term mercado-item-title">Blog</a>
                            </li>											
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>