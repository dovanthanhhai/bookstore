@extends('frontend.layout.layout')
@section('title',$title ?? '')
@section('content')
    
<!--main area-->
<main id="main" class="main-site">

    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="" class="link">home</a></li>
                <li class="item-link"><span>Product detail</span></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 main-content-area">
                <div class="wrap-product-detail">
                    <div class="detail-media">
                        <div class="product-gallery">
                            <ul class="slides">
                                <li  data-thumb="{{ asset('products/' . $products->image_1) }}">
                                    <img src="{{asset('products/'. $products->image_1)}}" alt="product thumbnail" />
                                </li>
                                @if (!empty($products->product_image))
                                    @foreach ($products->product_image as $Img)
                                        <li value="{{ $Img->product_id }}" data-thumb="{{ asset('productImg/' . $Img->image) }}">
                                            <img src="{{asset('productImg/'. $Img->image)}}" alt="product thumbnail" />
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="detail-info">
                        <h2 class="product-name">{{ $products->name }}
                            <div class="autho">
                                <h2 style="font-size: 14px;"><span style="font-size: 14px; font-weight: 700;">Author:</span>{{ $products->author }}</h2>
                            </div>
                            <div class="product-rating">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <a href="#" class="count-review">(05 review)</a>
                            </div>
                        </h2>
                        
                        <div class="wrap-social">
                            {{-- <a class="link-socail" href="#"><img src="" alt=""></a> --}}
                        </div>
                        <div class="wrap-price"><span class="product-price">${{number_format($products->price)}}</span></div>
                        <div class="stock-info in-stock">
                            <p class="availability">Availability: <b>In Stock</b></p>
                        </div>
                        <div class="quantity">
                            <span>Quantity:</span>
                            <div class="quantity-input">
                                <input type="text" name="product-quatity" id="productQty" value="1" data-max="120" pattern="[0-9]*" >
                                <input type="hidden" id="productId" value="{{ $products->id }}" >
                                <a class="btn btn-reduce" href="#"></a>
                                <a class="btn btn-increase" href="#"></a>
                            </div>
                        </div>
                        <div class="wrap-butons">
                            <button style="width: 200px;"  class="btn add-to-cart">Add to Cart</button>
                            
                            {{-- <div class="wrap-btn">
                                <a href="#" class="btn btn-compare">Add Compare</a>
                                <a href="#" class="btn btn-wishlist">Add Wishlist</a>
                            </div> --}}
                        </div>
                    </div>
                    <div class="advance-info">
                        <div class="tab-control normal">
                            <a href="#description" class="tab-control-item active">description</a>
                            <a href="#add_infomation" class="tab-control-item">Bindings Info</a>
                        </div>
                        <div class="tab-contents">
                            <div class="tab-content-item active" id="description">
                                <br>
                                {{ $products->description ??''}}
                                <br>
                            </div>
                            <div class="tab-content-item " id="add_infomation">
                                <a href=" {{ $products->url ??''}}"> {{ $products->url ??''}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end main products area-->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 sitebar">
                <div class="widget widget-our-services ">
                    <div class="widget-content">
                        <ul class="our-services">

                            <li class="service">
                                <a class="link-to-service" href="#">
                                    <i class="fa fa-truck" aria-hidden="true"></i>
                                    <div class="right-content">
                                        <b class="title">Free Shipping</b>
                                        <span class="subtitle">On Oder Over $10</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div><!-- Categories widget-->
            </div><!--end sitebar-->

            {{-- comment --}}
            <div class="single-advance-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row cmt-bk">
                    <div class="col-md-12 bk-item">
                        @if (!empty($products->Product_cmt))
                            <div class="item-cmt row">
                                @foreach ($products->Product_cmt as $cmt)
                                    <div class="col-md-1 cmt-img">
                                        <a href="#">
                                        <img src="{{ asset('images/user.png') }}" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-11 cmt-mess ">
                                        <p class="cmt"><span style="font-weight: 500;font-size: 16px;">
                                            <span style="font-weight: 600px">{{$cmt->Username->name ??''}}</span>
                                            <br>
                                            {{ $cmt->content }}
                                        </p>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        {{-- form comment --}}
                        <form action="{{ route('product-cmt.store') }}" method="post">
                            @csrf
                            <div class=" col-md-1 cmt-img">
                                <img src="{{ asset('images/user.png') }}" alt="">
                            </div>
                            <div class="form-cmt">
                                <input type="hidden" name="url" value="{{route('product-details',['any'=>$products->url, 'id'=>$products->id])}}">
                                <input type="hidden" name="product_id" value="{{ $products->id }}">
                                <input type="text" class="bk-form" name="content" required placeholder="write a comment....">
                                <button type="submit" class="send-cmt btn btn-primary"><i class="fa fa-paper-plane"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="single-advance-box col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="wrap-show-advance-info-box style-1 box-in-site">
                    <h3 class="title-box">Book </h3>
                    <div class="wrap-products">
                        <div class="products slide-carousel owl-carousel style-nav-1 equal-container" data-items="5" data-loop="false" data-nav="true" data-dots="false" data-responsive='{"0":{"items":"1"},"480":{"items":"2"},"768":{"items":"3"},"992":{"items":"3"},"1200":{"items":"5"}}' >
                            @foreach ($relateProducts as $item)
                                <div class="product product-style-2 equal-elem ">
                                    <div class="product-thumnail">
                                        <a href="{{route('product-details',['any'=>$item->url, 'id'=>$item->id])}}" title="{{ $item->name_1 }}">
                                            <figure><img src="{{ asset('/products/' . $item->image_1) }}" width="214" height="214" alt="{{$item->name}}"></figure>
                                        </a>
                                        <div class="group-flash">
                                            <span class="flash-item new-label">new</span>
                                        </div>
                                        <div class="wrap-btn">
                                            <a href="#" class="function-link">quick view</a>
                                        </div>
                                    </div>
                                    <div class="product-info">
                                        <a href="{{route('product-details',['any'=>$item->url, 'id'=>$item->id])}}" class="product-name"><span>{{ $item->name }}</span></a>
                                        <div class="wrap-price"><span class="product-price">${{ number_format($item->price) }}</span></div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div><!--End wrap-products-->
                </div>
            </div>

        </div><!--end row-->

    </div><!--end container-->

</main>
<!--main area-->

@endsection