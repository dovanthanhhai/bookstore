<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Config;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(Request $request){
        $login = Auth::user();
        $order = 0;
        if (!empty($login)) {
            // $orders = Orders::where('user_id',Auth::user()->id)->orderBy('updated_at', 'desc')->get();
            $order = Orders::where('user_id',Auth::user()->id)->count('id');
        }
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $config = Config::all();
        $featuredProducts = Products::where('category_id', '4')->orderBy('updated_at', 'desc')->get();
        $latestProducts = Products::where('category_id', '7')->orderBy('updated_at', 'desc')->limit(8)->get();
        $stationery = Products::where('category_id', '8')->orderBy('updated_at', 'desc')->get();
        $book = Products::where('category_id', '9')->orderBy('updated_at', 'desc')->get();
        $promotion = Products::where('category_id', '10')->orderBy('updated_at', 'desc')->get();
        $sliders = Slider::all();
        $categories = Category::all();
        $title = 'Book Library';
        return view('index',['title' => $title],compact(
            'config',
            'latestProducts',
            'featuredProducts',
            'sliders',
            'stationery',
            'promotion',
            'book',
            'cart_number',
            'login',
            'categories',
            'order',
            // 'orders'
        ));
    }

    public function search(Request $request)
    {
        $title = 'Book Library';
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $config = Config::all();
        $key = $request->input('key');
        $category = $request->input('category');
        $categories = Category::all();
        $query = Products::where('name', 'like' , "%$key%");
        if (!empty($category)){
            $query = $query->where('category_id', $category);
        }
        $query = $query->get();
        //     // $query = Products::join('categories','categories.id','=' ,'products.category_id')->where('')->where('products.name', 'like' , "%$key%")->get();
        return view('frontend.search',['title' => $title],compact(
            'query',
            'cart_number',
            'config',
            'categories'
        ));
    }

    public function contactus(Request $request)
    {
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $title = 'Book Library | Contact Us';
        return view('frontend.contactus',['title' => $title],compact('config', 'cart_number','categories','login'));
    }

    public function aboutus(Request $request){
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $title = 'About';
        return view('frontend.aboutus',['title' => $title],compact('config','cart_number','categories','login'));
    }

    public function productDetails($any ,$id ,Request $request){
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $title = 'Detail';
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $products = Products::find($id);
        $relateProducts = Products::where('id', '!=',$id)->orderBy('updated_at', 'desc')->limit(8)->get();
        return view('frontend.product-details',['title' => $title],compact(
            'config',
            'products',
            'relateProducts',
            'cart_number',
            'categories',
            'login'
        ));
    }
    public function comic(Request $request){
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $featuredProducts = Products::where('category_id', '4')->orderBy('updated_at', 'desc')->get();
        $title = ' Library | Comic';
        return view('frontend.product.product-seeMore.comic',['title' => $title],compact(
            'config',
            'featuredProducts',
            'cart_number',
            'categories',
            'login'
        ));
    }

    public function stationery(Request $request){
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $stationery = Products::where('category_id', '8')->orderBy('updated_at', 'desc')->get();
        $title = 'Library | Stationery';
        return view('frontend.product.product-seeMore.stationery',['title' => $title],compact(
            'config',
            'stationery',
            'cart_number',
            'categories',
            'login'
        ));
    }

    public function magazine(Request $request){
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $magazine = Products::where('category_id', '7')->orderBy('updated_at', 'asc')->get();
        $title = 'Library | Magazine';
        return view('frontend.product.product-seeMore.magazine',['title' => $title],compact(
            'config',
            'magazine',
            'cart_number',
            'categories',
            'login'
        ));
    }

    public function book(Request $request){
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $book = Products::where('category_id', '9')->orderBy('updated_at', 'desc')->get();
        $title = 'Library | Book';
        return view('frontend.product.product-seeMore.book',['title' => $title],compact(
            'config',
            'book',
            'cart_number',
            'categories',
            'login'
        ));
    }

    public function promotion(Request $request){
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $promotion = Products::where('category_id', '10')->orderBy('updated_at', 'desc')->get();
        $title = 'Library | Promotion';
        return view('frontend.product.product-seeMore.promotion',['title' => $title],compact(
            'config',
            'promotion',
            'cart_number',
            'categories',
            'login'
        ));
    }

    public function Faq(Request $request){
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $title = 'Library | Promotion';
        return view('frontend.faq',['title' => $title],compact(
            'config',
            'cart_number',
            'categories',
            'login'
        ));
    }

    public function blog(Request $request){
        $login = Auth::user();
        $config = Config::all();
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $blog = Products::where('status','new')->orderBy('updated_at', 'desc')->get();
        $cart_number = count($cart);
        $title = 'Library | News';
        return view('frontend.blog',['title' => $title],compact(
            'config',
            'cart_number',
            'categories',
            'login',
            'blog'
        ));
    }

    public function history(Request $request){
        $login = Auth::user();
        $config = Config::all();
        $orders = 0;
        if (!empty($login)) {
            $orders = Orders::where('user_id',Auth::user()->id)->orderBy('updated_at', 'asc')->get();
        }
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $title = 'Library | Purchase history';
        return view('frontend.MyAccount.purchase',['title' => $title],compact(
            'config',
            'cart_number',
            'categories',
            'login',
            'orders'
        ));
    }
    public function histotyDeitail(Request $request,$id)
    {
        $login = Auth::user();
        $config = Config::all();
        $orders = 0;
        if (!empty($login)) {
            $orders = Orders::where('user_id',Auth::user()->id)->orderBy('updated_at', 'desc')->get();
        }
        $order = Orders::find($id);
        $categories = Category::all();
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        $title = 'Library | Purchase history';
        return view('frontend.MyAccount.detailorder',['title' => $title],compact(
            'config',
            'cart_number',
            'categories',
            'login',
            'orders',
            'order'
        ));
    }
}
