<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Models\Order_detail;
use App\Models\Orders;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $config = Config::all();
        $user = Auth::user();
        if (!$request->session()->has('cart')) {
            return redirect()->route('index');
        }
        $cart = $request->session()->get('cart');
        $total = 0;
        if (!empty($cart)) {
            foreach($cart as $item){
                $total += $item->quantity* $item->price;
            }
        }
        $title = 'Checkout';
        return view('frontend.checkout',['title' => $title],compact('config','total','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orders = new Orders();
        $orders->user_id = Auth::user()->id;
        $orders->user_name = $request->user_name;
        $orders->user_phone = $request->user_phone;
        $orders->user_address = $request->user_address;
        $orders->user_email = $request->user_email;
        $orders->total_price = $request->total_price;
        $orders->shipping_cost = $request->shipping_cost;
        $orders->status_payment = $request->status_payment;
        DB::beginTransaction();
        try {
            if ($orders->save()){
                $cart = $request->session()->get('cart');
                if (!empty($cart)) {
                    foreach($cart as $item){
                        Order_detail::insert([
                            'orders_id' => $orders->id,
                            'product_id' => $item->id,
                            'price' => $item->price,
                            'quantity' => $item->quantity,
                        ]);
                    }
                }
            }
            DB::commit();;
        } catch (\Throwable $th) {
            DB::rollBack();
        }
        $request->session()->forget('cart');
        return redirect()->route('index');

    }

    public function orderDetail (Request $request ,$id){
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function show(Orders $orders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit(Orders $orders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orders $orders)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orders $orders,$id)
    {
        //
    }
}
