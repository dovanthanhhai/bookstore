<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\CartItem;
use App\Models\Config;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function cart(Request $request){
        // $request->session()->forget('cart');
        $login = Auth::user();
        $config = Config::all();
        $title = 'Shopping Cart';
        $cart = [];
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        }
        $cart_number = count($cart);
        return view('frontend.cart ',['title' => $title],compact('config', 'cart','cart_number','login'));
    }
    public function addCart(Request $request){
        $id = $request->pId;
        $qty = $request->qty;
        $product = Products::find($id);
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        } else {
            $cart = [];
        }
       // xử lý thêm số lượng nếu item đã có trong cart
       $item = null;
       foreach ($cart as $elem){
            if ($elem->id === $id ) {
                $item = $elem;
                break;
            }
       }
       if (!empty($item)) {
           $item->quantity += $qty;
       }
       else{
           $data_add = [
            'id' => $id,
            'name' => $product->name,
            'qty' => $qty,
            'price' => $product->price,
            'image' => $product->image_1,
            'url' => $product->url,
           ];
            $item = new CartItem($data_add);
            // them vao gio hang
            $cart[] = $item;
       }
       $request->session()->put('cart', $cart);
       return [
           'status' => true,
           'cart' => $cart
       ];
    }

    public function deleteCartItem (Request $request){
        $id = $request->pid;
        $key = null;
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');

            for ($i = 0; $i < count($cart); $i++){
                if ($cart[$i]->id == $id ) {
                    $key = $i;
                    break;
                }
           }
           if (isset($key)) {
            array_splice($cart, $key, 1);
           }
           $request->session()->put('cart', $cart);
        }
        return ['status' => true];
    }

    public function deleteCart(Request $request)
    {
        $request->session()->forget('cart');
        return redirect()->route('delete-cart');
    }

    public function changCartQuantity (Request $request){
        $id = $request->pId;
        $quantity = $request->quantity;
        if ($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');

            foreach ($cart as $elem){
                if ($elem->id == $id ) {
                    $elem->quantity = $quantity;
                    break;
                }
           }
           $request->session()->put('cart', $cart);
        }
        return ['status' => true];
    }
}
