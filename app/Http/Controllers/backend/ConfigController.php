<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = Orders::all()->count('id');
        $config = Config::all();
        $title = 'Admin | Config';
        return view('admin.config.index',['title' => $title], compact('config','order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $order = Orders::all()->count('id');
        return view('admin.config.config',compact('order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $config = $request->all();
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();            
            if ($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png') {
                return redirect()->route('config.config');
            }
            
            $imgName = $file->getClientOriginalName();
            $file->move('images', $imgName);
            $config['logo'] = $imgName;
        }
        Config::create($config);
        return redirect()->route('config.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function show(Config $config)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $config = Config::find($id);
        $title = 'Admin | Edit Config';
        $order = Orders::all()->count('id');
        return view('admin.config.update',['title' => $title], compact('order','config'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $config = Config::find($id);
        $config->webname = $request->input('webname');
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension();            
            if ($extension != 'jpg' && $extension != 'jpeg' && $extension != 'png') {
                return redirect()->route('config.config');
            }
            $imgName = $file->getClientOriginalName();
            $file->move('images', $imgName);
            $config['logo'] = $imgName;
        }
        $config->hotline = $request->input('hotline');
        $config->facebook = $request->input('facebook');
        $config->zalo = $request->input('zalo');
        $config->address = $request->input('address');
        $config->email = $request->input('email');
        $config->save();
        Session::flash('message', "Successfully deleted");
        return redirect()->route('config.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function destroy(Config $config)
    {
        if ($config->logo != null) {
            if (file_exists('images/' . $config->logo)) {
                unlink('images/' . $config->logo);
            }
        }
        $config->delete();
        return redirect()->route('config.index');
    }
}
