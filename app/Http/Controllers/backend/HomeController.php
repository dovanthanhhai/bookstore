<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Config;
use App\Models\Contact;
use App\Models\Order_detail;
use App\Models\Orders;
use App\Models\Products;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;

class HomeController extends Controller
{
    public function loginAdmin(){
        return view ('admin.loginAdmin');
    }

    public function dashboard(){
        $config = Config::all();
        $order = Orders::all()->count('id');
        $product = Products::all()->count('id');
        $user  = User::where('roles','=','customer')->count('id');
        $total_sale = Order_detail::all()->sum('price');
        $qty = Order_detail::all()->sum('quantity');
        $admin = Auth::user()->id;
        $title = 'Admin | Dashboard';
        return view('admin.dashboard',['title' => $title],compact('order','product','user','admin','config','total_sale','qty'));
    }

    public function processLogin(Request $request) 
    {
        if (!empty(auth()->user())){
            return redirect()->route('index');
        }
        $username = $request->username;
        $pass = $request->password;
        $login = array (
            'username' => $request->username,
            'password' => $request->password
        );
        if(Auth::attempt(['username' => $username, 'password' => $pass])){
            return redirect()->route('dashboard');
        }
        else{
            // $request->session()->flash('Error', 'Email hoặc mật khẩu không đúng!');
            return redirect()->route('admin.login');
        }
    }
    public function adminIndex(){
        $config = Config::all();
        $order = Orders::all()->count('id');
        $users = User::where('roles','admin')->orderBy('updated_at', 'desc')->limit(8)->get();
        $title = 'Admin | Admin';
        return view('admin.account.accountAdmin' , ['title' => $title],compact('users','order','config'));
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('admin.login');
    }

    public function detailCustomer(Request $request ,$id){
        $config = Config::all();
        $users = User::find($id);
        return view('admin.account.detailCustomer', compact('users','config'));
    }

    public function OrderDetail($id)
    {
        $config = Config::all();
        $orders = Orders::find($id);
        $order = Orders::all()->count('id');
        $title = 'Admin | Order Detail';
        return view ('admin.order.detail',['title' => $title], compact('order','orders','config'));
    }
    public function contact()
    {
        $contact = Contact::all()->sortByDesc('id');
        $config = Config::all();
        $order = Orders::all()->count('id');
        $title = 'Admin | Contact';
        return view('admin.contact.contact' , ['title' => $title],compact('order','config','contact'));
    }
}
