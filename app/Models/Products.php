<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'name',
        'author',
        'price',
        'price_sale',
        'description',
        'pub_company',
        'date_publishing',
        'publisher',
        'status',
        'url',
        'image_1',
        'image_2',
    ];
    public function Product_image(){
        return $this->hasMany(Product_image::class , 'product_id');
    }
    public function Product_cmt(){
        return $this->hasMany(Product_comment::class , 'product_id');
    }
}