<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;
    protected $fillable =[
        'user_id',
        'user_name',
        'user_phone',
        'user_address',
        'user_email',
        'total_price',
        'shipping_cost',
        'status_payment'
    ];
    public function orderDetail (){
        return $this->hasMany(Order_detail::class ,'orders_id');
    }
}
